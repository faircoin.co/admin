## Installation instructions new server


### prepare server as root
~~~
### change initial root password
passwd

### enable Firewall
ufw allow 22        # IMPORTANT: dont forget to allow this port 22 before enable ufw because this port is the port for ssh access !!!
ufw allow 80        # http://
ufw allow 443       # https://
ufw enable
ufw status
ufw disable

### install packages
apt-get update
apt-get install net-tools
apt-get install python3-pip
apt-get install docker-ce
apt-get install unzip
python3 -m pip install docker-compose



### add new user account(s)
adduser --shell /bin/bash username

###### add user to sudo group for root access ( only for server admins )
usermod -a -G sudo username

###### upload ssh key ( optional, users local machine ) https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys--2
ssh-copy-id demo@example.comm



### change hostname https://linuxize.com/post/how-to-change-hostname-on-ubuntu-18-04/
hostnamectl
hostnamectl set-hostname server.faircoin.co
nano /etc/hosts
~~~

### install Gitlab Runner
~~~
### gitlab runner - install package
curl -LJO https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb
dpkg -i gitlab-runner_amd64.deb

### gitlab runner - install gitlab-runner service
gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
usermod -a -G docker gitlab-runner
nano /home/gitlab-runner/.bash_logout ### mark or remove the clear screen command if exists
passwd gitlab-runner            # set new account password for ssh access

### gitlab runner - registration and start
gitlab-runner register
gitlab-runner start
~~~


### install server host
~~~
### nginx
apt update
apt install nginx
systemctl status nginx
systemctl start nginx
systemctl stop nginx
systemctl reload nginx

### certbot
apt-get update
apt-get install software-properties-common
add-apt-repository universe
apt-get update
apt-get install certbot python3-certbot-nginx
certbot certonly --nginx --domain explorer.faircoin.co
~~~

### configure server host
~~~
### Workflow add/remove domains

#### 1 - add config file to /etc/nginx/sites-available/example.com ( templates are in this repository )

##### nginx
export DOMAIN=example.com && curl https://git.fairkom.net/faircoin.co/admin/-/raw/master/nginx | sed -e s/DOMAIN/${DOMAIN}/g > /etc/nginx/sites-available/${DOMAIN}

##### nginx proxy
export LH_PORT=4000 && export DOMAIN=example.com && curl https://git.fairkom.net/faircoin.co/admin/-/raw/master/nginx_proxy | sed -e s/DOMAIN/${DOMAIN}/g -e s/LH_PORT/${LH_PORT}/g > /etc/nginx/sites-available/${DOMAIN}

#### 2 - create ssl certificate
certbot certonly --nginx --domain ${DOMAIN}

#### 3 - enable domain
ln -s /etc/nginx/sites-available/${DOMAIN} /etc/nginx/sites-enabled/
systemctl reload nginx

#### disable domain
rm /etc/nginx/sites-enabled/example.com

~~~
